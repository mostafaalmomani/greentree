function loadConfig(callb:Function): any {
  let href: string = document.location.origin;
  if (document.location.href.toLowerCase().includes("admin")){
    href += '/admin';
  }
  const http = new XMLHttpRequest();
  http.onload = function() {
    const envObj: any =JSON.parse(this.responseText);
    environment.apiUrl = envObj.apiUrl;
    environment.production = envObj.production;
    environment.host = envObj.host,

    callb();
  }
  console.log(href);

http.open("GET", `${href}/assets/env.json`);
http.send();
}

export const environment = {
  init(callback:Function){
    loadConfig(callback);
  },
  apiUrl: null,
  production: null,
  host: null,

};
