import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  apiUrl = environment.host
  // apiUrl = 'http://localhost:80'
  constructor() { }
}
