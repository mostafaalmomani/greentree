import { Questions, Rules } from ".";

export class Exam {
  _id: string;
  examType: string;
  duration: string;
  start: Date;
  end: Date;
  semester: string;
  year: string;
  questionsCount: string;
  fullMark: string;
  isActive: boolean;
  questions: Questions;
  rules: Rules;
  courseName: string;
  courseNumber: string;
  scale: object;
  TimeSpent: Date;
  created: Date;
  teacherId: string;
  tokenLink: string;
  maxGreade: number;
  markForEachQ: number;
}
