export class Rules {
  _id: string;
  returnBack?: boolean;
  isRandomSequence?: boolean;
  numberOfPage?: number;
  maxGreade?: number;
  examLanguage?: string;
  resultVisable?: boolean;
  incorrectAnswersIsVisable?: number;
  isRequiredQusetions?: boolean;
}
