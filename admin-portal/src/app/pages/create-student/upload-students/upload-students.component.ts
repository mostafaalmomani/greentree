import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StudentServicesService } from 'src/app/services/student-services.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-upload-students',
  templateUrl: './upload-students.component.html',
  styleUrls: ['./upload-students.component.scss']
})
export class UploadStudentsComponent implements OnInit {
  willDownload = false;
  displayedColumns: string[] = ['fullName', 'email', 'purePassword', 'phoneNumber', 'level'];
  public dataSource;

  constructor(private studentServicesService: StudentServicesService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }


  onFileChange(ev) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = jsonData;
      // document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
      this.setDownload(dataString);
    }
    reader.readAsBinaryString(file);
  }

  setDownload(data) {

    let sheet = Object.keys(data);
    console.log(sheet);
    this.willDownload = true;
    this.dataSource = data[sheet[0]];
    // setTimeout(() => {
    //   const el = document.querySelector("#download");
    //   el.setAttribute("href", `data:text/json;charset=utf-8,${encodeURIComponent(data)}`);
    //   el.setAttribute("download", 'xlsxtojson.json');
    // }, 1000)
  }

  addStudents() {
    this.studentServicesService.addStudents(this.dataSource)
      .subscribe(arg => {
        console.log(arg);
        if (arg.message.match('saved!')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      },
        (error) => {
          console.log(error);
          if (error) {
            this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
          }
        });

  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
