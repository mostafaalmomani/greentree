import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Exam, Student } from 'src/app/core/models';
import { ExamsService } from 'src/app/services/exams.service';
import { MessageDialogComponent } from 'src/app/shared/message-dialog/message-dialog.component';
import { StudentsListComponent } from '../students-list/students-list.component';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-view-exam',
  templateUrl: './view-exam.component.html',
  styleUrls: ['./view-exam.component.scss']
})

export class ViewExamComponent implements OnInit , AfterViewInit {
  displayedColumns: string[] = [
    'teacherName', 'courseName', 'courseNumber',
    'isConfimed', 'confirm', 'students'
  ];
  public dataSource;
  public term: any;
  public cousreNumbers: Set<any> = new Set();
  public levels = ['اول', 'ثاني', 'نصفي', 'نهائي'];
  public exams: Exam[];
  public student: Student[];
  source = timer(0, 20000);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private examsService: ExamsService, public dialog: MatDialog,
    private _snackBar: MatSnackBar) {
      this.dataSource = new MatTableDataSource();
     }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.subscription = this.source.subscribe(val => {
      this.loadRequests();
    });
  }

  private loadRequests() {
    this.examsService.getRequests()
      .subscribe(arg => {
        console.log(arg);
        this.dataSource.data = arg.requests;
      });
  }

  courseNumberFilter(e): void {
    const temStudents = this.exams;
    this.dataSource.data = this.exams.filter(exam => exam.courseNumber === e.value);
  }

  kindOfExamFilter(e): void {
    const temStudents = this.exams;
    this.dataSource.data = this.exams.filter(exam => exam.examType === e.value);
  }

  getStudents(exam: Exam): void {
    const dialogRef = this.dialog.open(StudentsListComponent, {
      width: '1700px',
      height: '510px',
      data: { mode: 1, examObject: exam }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.subscription = this.source.subscribe(val => {
          this.loadRequests();
          this.subscription.unsubscribe();
        });
      }
    });
  }

  confirm(course: any): any {

    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '230px',
      height: '210px',
      data: { title: 'رسالة تأكيد!', bodyMessage: 'هل أنت متأكد من الاعتماد' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.examsService.consfirm(course)
          .subscribe(arg => {
            if (arg.Confirmed) {
              this.openSnackBar('تم الأعتماد', 'suc-snackbar');
              this.subscription = this.source.subscribe(val => {
                this.loadRequests();
                this.subscription.unsubscribe();
              });
            } else {
              this.openSnackBar('حدث خطأ', 'err-snackbar');
            }
          }, (err) => {
            console.log(err);
            this.openSnackBar('حدث خطأ', 'err-snackbar');
          });
      }
    });

  }

  ngOnDestroy(): any {
    this.subscription.unsubscribe();
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
