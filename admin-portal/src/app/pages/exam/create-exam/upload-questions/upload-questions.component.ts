import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateExamComponent } from '../create-exam.component';
import { FormControl, FormArray, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Exam, Rules } from 'src/app/core/models';
import { ExamsService } from 'src/app/services/exams.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TestBankService } from 'src/app/services/test-bank.service';

@Component({
  selector: 'app-upload-questions',
  templateUrl: './upload-questions.component.html',
  styleUrls: ['./upload-questions.component.scss']
})
export class UploadQuestionsComponent implements OnInit {
  file: any;
  formArrayOfQuestions: FormArray = new FormArray([]);
  qFormGroup: FormGroup;
  explanations: any[] = [];
  point: any[] = [];
  currentExam: Exam;
  currentRules: Rules;
  uploadedFiles: Array<File> = [];
  showPreview = false;
  MODE: any;
  constructor(
    public dialogRef: MatDialogRef<CreateExamComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private examsServices: ExamsService, private _snackBar: MatSnackBar
    , private testBankServices: TestBankService) { }

  ngOnInit(): void {
    this.currentExam = this.data.currentExam;
    this.currentRules = this.data.currentRules;
    this.MODE = this.data.MODE;
  }

  uploadTxtFile(): any {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      const data = fileReader.result.toString();
      const lines = data.split(/[\r\n]+/g);
      for (let i = 0; i < lines.length; i++) {
        if (lines[i].includes('<Q>')) {

          this.qFormGroup = this.fb.group({
            questionText: [null, Validators.required],
            point: [null, Validators.required],
            answers: this.fb.array([])
          });

          const name: string = lines[i].split('>')[1];
          this.qFormGroup.get('questionText').setValue(name);
          this.qFormGroup.get('point').setValue(this.currentExam.markForEachQ);
        }
        else
          if (lines[i].includes('<C>')) {
            const name: string = lines[i].split('>')[1];
            const ansFormGroup = this.fb.group({
              answer: [false, Validators.required],
              text: [name, Validators.required]
            });
            const AnswersFG = this.qFormGroup.get('answers') as FormArray;
            AnswersFG.push(ansFormGroup);
          }
          else
            if (lines[i].includes('<C+>')) {
              const name: string = lines[i].split('>')[1];
              const ansFormGroup = this.fb.group({
                answer: [true, Validators.required],
                text: [name, Validators.required]
              });
              const AnswersFG = this.qFormGroup.get('answers') as FormArray;
              AnswersFG.push(ansFormGroup)
            }
        if (i % 5 === 0) {
          this.formArrayOfQuestions.push(this.qFormGroup);
        }
      }

      const Qs = this.formArrayOfQuestions.value;
      let i = 0;
      Qs.forEach(element => {
        this.point[i]
        //=this.currentExam.markForEachQ;
        this.explanations[i];
        i++;
      });
      this.point.forEach(element => {
        this.point[i].push(this.currentExam.markForEachQ);
      });
      this.showPreview = true;
    }
    fileReader.readAsText(this.file);
  }

  fileChanged(e): any {
    this.file = e.target.files[0];
  }

  fileChange(element, index): any {
    this.uploadedFiles[index] = (element.target.files[0]);

  }

  savePoint(point, qousetionObject, index, explanation): any {

    const formData = new FormData();
    formData.append('rules', JSON.stringify(this.currentRules));
    formData.append('point', point);
    formData.append('examId', this.currentExam._id);
    formData.append('qousetionObject', JSON.stringify(qousetionObject));
    // const name = this.uploadedFiles.length > 0  ? this.uploadedFiles[index].name : '';
    if (this.uploadedFiles.length > 0) {
      formData.append('image', this.uploadedFiles[index], this.uploadedFiles[index].name);
    }
    if (this.MODE === 'regular_exam') {
      this.examsServices.saveQuestion(formData).subscribe(arg => {
        this.uploadedFiles = [];
        console.log(this.uploadedFiles.length);
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
    } else {
      formData.append('explanation', explanation);
      this.testBankServices.saveQuestion(formData).subscribe(res => {
        this.uploadedFiles = [];
        console.log(res);
      });
    }


  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
