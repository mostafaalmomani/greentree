import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Student } from 'src/app/core/models';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { MessageDialogComponent } from 'src/app/shared/message-dialog/message-dialog.component';
import { StudentCardAddEditComponent } from './student-card-add-edit/student-card-add-edit.component';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit , AfterViewInit{
  displayedColumns: string[] = ['fullName', 'email', 'purePassword', 'phoneNumber', 'joined', 'update', 'delete'];
  public dataSource;
  hide = true;
  @ViewChild('htmlData') htmlData:ElementRef;
  head = [['Name', 'Email','Password', 'Phone Number']];
  data = [];
  source = timer(0, 30);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private fb: FormBuilder, public dialog: MatDialog,
    private studentservice: StudentServicesService, private _snackBar: MatSnackBar,private cdr: ChangeDetectorRef) {
      this.dataSource = new MatTableDataSource([]);
     }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.loadTeacher();

  }


  private loadTeacher() {
    this.studentservice.getTeachers()
      .subscribe(arg => {
        console.log(arg.techers);
        arg.teachers.forEach(element => {
          let dataObj = [];
          dataObj.push();
          dataObj.push(element.fullName);
          dataObj.push(element.email);
          dataObj.push(element.purePassword === undefined ? 'misk' : element.purePassword);
          dataObj.push(element.phoneNumber);
          this.data.push(dataObj);
          dataObj = [];
        });
        console.log(this.data);
        this.dataSource.data = arg.teachers;
        this.dataSource.paginator = this.paginator;
        this.cdr.detectChanges();
      });
  }

  addStudent(): void {
    const dialogRef = this.dialog.open(StudentCardAddEditComponent, {
      width: '500px',
      height: '510px',
      data: { mode: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        window.location.reload();
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  edit(student: Student): void {
    const dialogRef = this.dialog.open(StudentCardAddEditComponent, {
      width: '500px',
      height: '510px',
      data: { mode: 1, studentObject: student }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        window.location.reload();
      }
    });
  }

  delete(_id: string): void {
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '230px',
      height: '210px',
      data: { title:  'رسالة تأكيد!', bodyMessage: 'هل أنت متأكد من الحذف؟' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.studentservice.deleteTeacher(_id)
          .subscribe(arg => {
            console.log(arg);
            this.openSnackBar('تم الحذف', 'suc-snackbar');
            this.subscription = this.source.subscribe(val => {
              this.loadTeacher();
              this.subscription.unsubscribe();
            });

          });

      }
    });
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

}
