function loadConfig(callb:Function): any {
  let href: string = document.location.origin;
  if (document.location.href.toLowerCase().includes("teacher")){
    href += '/teacher';
  }
  const http = new XMLHttpRequest();
  http.onload = function() {
    const envObj: any =JSON.parse(this.responseText);
    environment.apiUrl = envObj.apiUrl;
    environment.production = envObj.production;
    environment.filesUrl = envObj.filesUrl;

    callb();
  }
  console.log(href);

http.open("GET", `${href}/assets/env.prod.json`);
http.send();
}

export const environment = {
  init(callback:Function){
    loadConfig(callback);
  },
  apiUrl: null,
  filesUrl: null,
  production: null,

};
