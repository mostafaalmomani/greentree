import { ThrowStmt } from '@angular/compiler';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Student } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { Mark } from 'src/app/core/models/mark';
import { MarkCourse } from 'src/app/core/models/markCourse';
import { ExamsService } from 'src/app/services/exams.service';
import { StudentGradesComponent } from '../student-grades/student-grades.component';
import { timer, Subscriber, Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-student-grade-view',
  templateUrl: './student-grade-view.component.html',
  styleUrls: ['./student-grade-view.component.scss']
})
export class StudentGradeViewComponent implements OnInit,AfterViewInit {

  courseId: any;
  public dataSource;
  public assesment: any = [];
  public courseName = '';
  public students: Student[] = [];
  public originalStudents: Student[] = [];
  public studentMark: any[] = [];
  public studentOtherMark: any[] = [];
  public points: any[] = [];
  public markEntity: MarkCourse[] = [];
  public otherMarkEntity: Mark[] = [];
  public term: any;
  public semester: any;
  public year: any;

  course: Course;
  public levels = ['1', '2', '3', '4', '5', '6', 'أخرى'];
  displayedColumns: string[] =
    ['studentName', 'studentNumber', 'level', 'email','total', 'editOtherMark','degree',  'otherMark'];
  property: any;
  sum: any;
  source = timer(200, 2000);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private route: ActivatedRoute, private examsService: ExamsService,
    public dialog: MatDialog, private _snackBar: MatSnackBar) {
      this.dataSource = new MatTableDataSource(this.assesment);

     }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.courseId = params.id;
      this.loadAssessments();
      this.loadMarks();
    });
  }

  private loadAssessments() {
    this.examsService.getStudentAssessments(this.courseId)
      .subscribe(arg => {
        console.log(arg.assessments);
        arg.assessments.forEach(element => {
          this.studentMark[element.studentsId._id] = element.finalSum;
          this.studentOtherMark[element.studentsId._id] = element.otherExamMark;
          this.points[element.studentsId._id] = this.findRangePoint(element.course.scales, element.finalSum);
        });
        this.assesment = arg.assessments;
        this.semester = arg.assessments[0].semester;
        this.year = arg.assessments[0].year;
        this.dataSource.data = this.assesment;
      });
  }

  findRangePoint(points: any[], sum: any): any {

    let total = sum;
    let degree = points.filter(point => (total >= parseFloat(point.min)) && (total <= parseFloat(point.max)));
    console.log('degree  ', degree);
    if (degree.length > 0) {
      return degree[0]['degree'];
    }
    return 'هـ';
  }

  private loadMarks() {
    debugger
    this.examsService.getCoursesStudents(this.courseId)
      .subscribe(arg => {
        this.courseName = arg.courseName;
        this.students = arg.students;
        this.originalStudents = arg.students;
        this.course = arg.course;
      });
  }

  private loadStudentsMarks() {
    this.examsService.getStudentMarks('123', this.course.courseNumber)
      .subscribe(arg => {
        console.log(arg);
        this.property = arg.marks
      });
  }

  openMark(studentId: string): void {
    this.examsService.getStudentMarks(studentId, this.courseId)
      .subscribe(arg => {
        const dialogRef = this.dialog.open(StudentGradesComponent, {
          width: '380px',
          height: '390px',
          data: { marks: arg.marks, courseId: this.courseId, student: studentId, course: this.course, courseName: this.courseName }
        });

        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          if (result) {
            // window.location.reload();
          }
        });
      });
  }

  saveOtherMark(): any {
    this.students.forEach(s => {
      const mark = new Mark();
      mark.courseNumber = this.course.courseNumber;
      mark.markDesc = 'others';
      mark.markPoint = Number.parseFloat(this.studentOtherMark[s._id]);
      mark.studentsId = s._id;
      this.otherMarkEntity.push(mark);
    });

    this.examsService.saveOtherMark(this.otherMarkEntity)
      .subscribe(arg => {
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
          this.subscription = this.source.subscribe(val => {
            this.loadAssessments();
            this.loadMarks();
            this.subscription.unsubscribe();
          });
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }

  saveCourseMarks(): any {
    this.students.forEach(s => {
      const markCourse = new MarkCourse();
      markCourse.year = this.year;
      markCourse.semester = this.semester;
      markCourse.courseNumber = this.course.courseNumber;
      markCourse.studentsId = s._id;
      markCourse.grade = Number.parseFloat(this.studentMark[s._id]);
      markCourse.markPoint = this.points[s._id]
      markCourse.markDesc = '';
      markCourse.courseName = this.course.courseName;
      markCourse.examStatus = this.findMarkStatus(this.course.scales,Number.parseFloat(this.studentMark[s._id]));
      this.markEntity.push(markCourse);
    });

    this.examsService.saveCourseMark(this.markEntity)
      .subscribe(arg => {
        console.log(arg);
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
          this.subscription = this.source.subscribe(val => {
            this.loadAssessments();
            this.loadMarks();
            this.subscription.unsubscribe();
          });
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    // console.log(filterValue);

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  findMarkStatus(points: any, total: number): any {

    // let total = this.sum;
    debugger
    let degree = points.filter(point => (total >= parseFloat(point.min)) && (total <= parseFloat(point.max)));
    console.log('degree  ', degree);
    if(degree[0] === undefined){
      return false;

    }
    if (degree[0]['degree'] != 'هـ') {
      return true;
    }

    return false;
  }
  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
