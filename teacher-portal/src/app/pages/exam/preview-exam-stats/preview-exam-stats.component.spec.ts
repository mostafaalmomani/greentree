import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewExamStatsComponent } from './preview-exam-stats.component';

describe('PreviewExamStatsComponent', () => {
  let component: PreviewExamStatsComponent;
  let fixture: ComponentFixture<PreviewExamStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviewExamStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewExamStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
