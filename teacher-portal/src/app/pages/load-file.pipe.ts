import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'loadFile'
})
export class LoadFilePipe implements PipeTransform {
  apiUrl = environment.filesUrl;


  constructor(private http: HttpClient) {
  }

  transform(value: any, ...args: any[]): any {
    const headers = new Headers({'Content-Type': 'image/*'});
    return `${this.apiUrl}readImage/${value}`;
  }

}
