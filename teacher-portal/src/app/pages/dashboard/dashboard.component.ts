import { Component, OnInit } from '@angular/core';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public username: string;
  constructor(private authService: AuthenticationServiceService) {
    this.authService.setAuthTimer(Number.parseInt(sessionStorage.getItem('duration')));
  }

  ngOnInit(): void {
    this.username = sessionStorage.getItem('fullName');
  }

}
