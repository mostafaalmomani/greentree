export class MarkCourse {
  _id?: string;
  markDesc: string;
  courseNumber: string;
  courseName: string;
  studentsId?: string;
  markPoint?: string;
  year?: string;
  grade?: number;
  semester?: number;
  examStatus?: boolean;
}
