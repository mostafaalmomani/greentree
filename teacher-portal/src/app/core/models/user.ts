export class User {
  id?: string;
  email: string;
  password: string;
  fullName?: string;
  token?: string;
  phoneNumber?: string;
  pictuer?: string;
}
