export class Course {
  _id?: string;
  courseName: string;
  courseNumber: string;
  scales?: object;
  teacherId?: string;
}
