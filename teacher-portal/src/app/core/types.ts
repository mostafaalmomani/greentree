export enum examTypes {
  FIRST = 1,
  SECOND = 2,
  FINAL = 3,
  OTHER = 4
}
