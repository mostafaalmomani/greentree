import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

   apiUrl = environment.apiUrl
//  apiUrl = "http://localhost:80/api/v1/"
//"http://138.68.71.102/api/v1/"
  constructor() { }
}
