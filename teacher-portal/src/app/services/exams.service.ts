import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Exam, Questions, Rules, Student } from '../core/models';
import { Course } from '../core/models/course';
import { Mark } from '../core/models/mark';
import { MarkCourse } from '../core/models/markCourse';
import { AuthenticationServiceService } from './authentication-service.service';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ExamsService extends BaseService{

  constructor(private http: HttpClient, private authservices: AuthenticationServiceService) {
    super();
  }


  createExam(exam: Exam, rules: Rules, students: Student): Observable<any> {
    return this.http.post<{ ok: any, exam: Exam, rules: Rules }>(`${this.apiUrl}createExam`, { exam, rules, students });
  }

  getExams(): Observable<any> {
    return this.http.post<{ exams: Exam, rules: Rules }>(`${this.apiUrl}getExam`, {});
  }

  getStudents(id: string): any {
    return this.http.get<{ students: Student[] }>(`${this.apiUrl}getStudents/${id}`);
  }

  getStudentsMark(id: string, examType: string): any {
    return this.http.get<{ marks: Mark[] }>(`${this.apiUrl}getStudentsMark/${id}/${examType}`);
  }

  saveExamMarks(mark: Mark[]): any {
    return this.http.post<{ message: string, mark: Mark }>(`${this.apiUrl}saveExamMarks`, { mark });
  }

  saveExamMark(mark: Mark): any {
    return this.http.post<{ message: string, mark: Mark }>(`${this.apiUrl}saveExamMark`, { mark });
  }

  saveCourseMark(marks: MarkCourse[]): any {
    return this.http.post<{ message: string }>(`${this.apiUrl}saveCourseMark`, { marks });
  }

  saveCourse(course: any): any {
    return this.http.post<{ message: string }>(`${this.apiUrl}createCourse`, { course });
  }

  editCourse(course: any): any {
    return this.http.post<{ message: string }>(`${this.apiUrl}editCourse`, { course });
  }

  getCourses(): any {
    return this.http.get<{ courses: Course }>(`${this.apiUrl}getCourse`);
  }

  getCoursesStudents(courseNumber: string): any {
    return this.http.get<{ students: any, courseName: string, course: Course }>(`${this.apiUrl}getCoursesStudents/${courseNumber}`);
  }

  getStudentMarks(studentId: string, courseNumber: string): any {
    return this.http.post<{ marks: Mark }>(`${this.apiUrl}getStudentMarks`, { studentId, courseNumber });
  }

  getStudentCourseMarks(studentId: string, courseNumber: string): any {
    return this.http.get<{ mark: MarkCourse }>(`${this.apiUrl}getStudentCourseMark/${studentId}/${courseNumber}`);
  }

  saveOtherMark(mark: Mark[]): any {
    return this.http.post<{ message: string, mark: Mark }>(`${this.apiUrl}saveOtherMark`, { mark });
  }

  saveQuestion(data: FormData): any {
    return this.http.post<{ question: Questions }>(`${this.apiUrl}saveQuestion`, data);
  }

  confirmAssement(confirmAssementObj, examType, courseNumber): any {
    return this.http.post<{ Confirmed: boolean }>(`${this.apiUrl}confirmAssement`,
      { confirmAssementObj: confirmAssementObj, examType: examType, courseNumber: courseNumber });
  }

  confirmFinalMark(courseNumber): any {
    return this.http.post<{ Confirmed: boolean }>(`${this.apiUrl}confirmMark`,
      { courseNumber: courseNumber });
  }

  getStudentsAnswers(examId: any): any {
    return this.http.get<{ answers: any }>(`${this.apiUrl}getStudentsFileAnswers/${examId}`);
  }
  getExamInfo(examId: string): any {
    return this.http.get<{ obj: any }>(`${this.apiUrl.replace('v1', 'v2')}getExamInfo/${examId}`);
  }

  getQusetion(examId: string): any {
    return this.http.get<{ obj: any }>(`${this.apiUrl.replace('v1', 'v2')}getQuestions/${examId}`);
  }

  getStudentAssessments(course: string): any {
    return this.http.post<{ assessments: any }>(`${this.apiUrl}getAssessments`, { course: course });
  }

  getStudentFileAnswers(path: any,studentNumber: any) {
    this.http
      .get(`${this.apiUrl}readImage/${path}`, { responseType: "blob" }) //set response Type properly (it is not part of headers)
      .toPromise()
      .then(blob => {
        let url = window.URL.createObjectURL(blob);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = `${studentNumber}.pdf`;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      })
      .catch(err => console.error("download error = ", err))
  }
}
