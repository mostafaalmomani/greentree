import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from './authentication-service.service';
import { LoaderService } from '../loader/loader.service';
import { finalize } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationServiceService,public loadrServices: LoaderService) { }

// xmlhttp.setRequestHeader("Access-Control-Allow-Origin","*");
// xmlhttp.setRequestHeader("Access-Control-Allow-Credentials", "true");
// xmlhttp.setRequestHeader("Access-Control-Allow-Methods", "GET");
// xmlhttp.setRequestHeader("Access-Control-Allow-Headers", "Content-Type");

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authToken = this.authService.getToken();

    const authRequest =  req.clone({
      headers:new HttpHeaders({
        'Authorization': 'Bearer ' + authToken,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type'
      })
    });
    return next.handle(authRequest);
  }
}
