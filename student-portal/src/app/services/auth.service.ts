import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Student } from '../core/models/';
import { map } from 'rxjs/operators';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService{
  private currentStudentSubject: BehaviorSubject<Student>;
  public currentStudent: Observable<Student>;
  private isAuthenticated = false;
  private token: string;
  private tokenTimer: any;
  private studentId: string;
  public studentName: string;
  private authStatusListener = new Subject<boolean>();
  public studentNumber: string;
  constructor(private http: HttpClient, private router: Router) {
    super();
    this.currentStudentSubject = new BehaviorSubject<any>(sessionStorage.getItem('currentStudent'));
    this.currentStudent = this.currentStudentSubject.asObservable();
  }

  public get currentStudentValue(): Student {
    return this.currentStudentSubject.value;
  }


  login(studentNumber: string, password: string): any {

    return this.http.post<{ student: Student }>(`${this.apiUrl}auth`,
      { studentNumber, password })
      .pipe(map(student => {
        // tslint:disable-next-line:no-string-literal
        if (student && student['token']) {
          // tslint:disable-next-line:no-string-literal
          const token = student['token'];
          this.token = token;
          if (token) {
            // tslint:disable-next-line:no-string-literal
            const expiresInDuration = student['expiresIn'];
            sessionStorage.setItem('duration', expiresInDuration);
            this.isAuthenticated = true;
            // tslint:disable-next-line:no-string-literal
            this.studentId = student['id'];
            // tslint:disable-next-line:no-string-literal
            this.studentName = student['studentName'];
            // tslint:disable-next-line:no-string-literal
            this.studentNumber = student['studentNumber'];
            this.authStatusListener.next(true);
            const now = new Date();
            const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
            this.saveAuthData(token, expirationDate, this.studentId, this.studentName);
            this.currentStudentSubject.next(student.student);

          }

        }

        return student;
      }));
  }
  getToken(): any {
    const token = sessionStorage.getItem('token');
    return token;
  }

  getIsAuth(): any {
    return this.isAuthenticated;
  }

  getStudentId(): any {
    return sessionStorage.getItem('currentStudent');
  }
  getAuthStatusListener(): any {
    return this.authStatusListener.asObservable();
  }


  private saveAuthData(token: string, expirationDate: Date, studentId: string, fullName: string): void {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('expiration', expirationDate.toISOString());
    sessionStorage.setItem('currentStudent', studentId);
    sessionStorage.setItem('fullName', fullName);
    sessionStorage.setItem('studentNumber', this.studentNumber);
  }

  public getAuthData(): any {
    const token = sessionStorage.getItem('token');
    const expirationDate = sessionStorage.getItem('expiration');
    const studentId = sessionStorage.getItem('currentStudent');
    if (!token || !expirationDate) {
      return;
    }
    return {
      token,
      expirationDate: new Date(expirationDate),
      studentId
    };
  }

  public setAuthTimer(duration: number): any {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 3000);
  }


  logout(): void {
    this.http.get<{ isLoggedOut: boolean }>(`${this.apiUrl}logout`).subscribe(res => {
      sessionStorage.removeItem('currentStudent');
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('expiration');
      sessionStorage.removeItem('fullName');
      sessionStorage.removeItem('studentNumber');
      sessionStorage.removeItem('duration');
      this.currentStudentSubject.next(null);
      this.router.navigate(['/auth/login']);
      // location.reload();
    });

  }
}
