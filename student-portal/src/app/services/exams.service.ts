import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Exam } from '../core/models';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ExamsService extends BaseService{
  summary = [];
  result;

  constructor(private http: HttpClient, private authServices: AuthService) {
    super();
  }


  getMarks(): any{
    return this.http.get<{obj: any}>(`${this.apiUrl}getStudentsMark/${this.authServices.getStudentId()}`);
  }

  getExamsForStudents(): any {//
    return this.http.get<{obj: Exam}>(`${this.apiUrl}getExamsForStudents/${this.authServices.getStudentId()}`);
  }

  getQusetion(examId: string): any {
    return this.http.get<{obj: any}>(`${this.apiUrl}getQuestions/${examId}`);
  }//

  getExamInfo(examId: string): any {
    return this.http.get<{obj: any}>(`${this.apiUrl}getExamInfo/${examId}`);
  }

  saveAnswers(answers: any, examId): any {
    return this.http.post<{obj: any}>(`${this.apiUrl}saveAnswers/${examId}`, {answers});
  }

  getMark(markId): any {
    return this.http.get<{obj: any}>(`${this.apiUrl}getMark/${markId}`);
  }

  isActive(examId): any {
    return this.http.get<{obj: any}>(`${this.apiUrl}isActive/${examId}`);
  }

  saveStudentAnswers(data: FormData): any {
    return this.http.post<{ message: any }>(`${this.apiUrl}saveSAnswers`, data);
  }
  setSummary(summary): any{
    this.summary = summary;
  }

  getSummary(): any{
    return this.summary;
  }

  setResult(result): any{
    this.result = result;
  }

  getResult(): any{
    return this.result;
  }
}
