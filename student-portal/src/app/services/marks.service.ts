import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { BaseService } from './base.service';
import { Filter } from '../core/models';

@Injectable({
  providedIn: 'root'
})
export class MarksService extends BaseService{

  constructor(private http: HttpClient, private authServices: AuthService) {
    super();
  }

  getCourses(courseNumber: any): any {
    return this.http.get(`${this.apiUrl}getCoursesStudents/${courseNumber}`);
  }
  getMarks(filter: Filter): any{
    return this.http.
    get<{assessments: any}>(`${this.apiUrl}getStudentsMark/${this.authServices.getStudentId()}?year=${filter.year}&semester=${filter.semester}`);
  }

  getCourse(courseNumber: any): any {
    return this.http.get<{course: any}>(`${this.apiUrl}getStudentsCourse/${courseNumber}`);
  }

  getExamMarks(courseNumber: any): any {
    return this.http.get<{marks: any}>(`${this.apiUrl}getStudentsExamMark/${courseNumber}/${this.authServices.getStudentId()}`);
  }

  getCoursesMark(filter: Filter): any {
    return this.http.get<{Markcourses: any}>(`${this.apiUrl}getCoursesMark?year=${filter.year}&semester=${filter.semester}`);
  }

  getOrder(): any {
    return this.http.get<{Markcourses: any}>(`${this.apiUrl}getOrder`);
  }
}
