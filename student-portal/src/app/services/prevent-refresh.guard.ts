import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ExamComponent } from '../pages/exam/exam.component';

@Injectable({
  providedIn: 'root'
})
export class PreventRefreshGuard implements CanDeactivate<ExamComponent> {
  canDeactivate(component: ExamComponent): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const confirmationMessage = window.confirm('لم ينتهي الامتحان سوف تفقد الامتحان اذا اجبت بنعم');
    return confirmationMessage;
  }

}
