export class StudentMarksObj {
  courseName?: string;
  courseNumber: string;
  firstExamMark: number;
  secondExamMark?: number;
  other?: number;
  total?: number;
  level?: string;
  markTotal?: number;
  markStatus?: string;
  hours?: number;
  markPoint?: string;
  courseStatus?: string;
}
