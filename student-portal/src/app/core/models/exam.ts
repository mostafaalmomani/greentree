import { Questions, Rules } from '.';

export class Exam {
  // tslint:disable-next-line:variable-name
  _id: string;
  examType: string;
  duration: string;
  start: string;
  end: string;
  semester: string;
  year: string;
  questionsCount: string;
  fullMark: string;
  isActive: boolean;
  questions: Questions;
  rules: Rules;
  courseName: string;
  courseNumber: string;
  scale: object;
  TimeSpent: Date;
  created: Date;
  teacherId: string;
  tokenLink: string;
  maxGreade: number;
}
