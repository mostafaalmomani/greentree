import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AppBlockCopyPasteDirective } from './app-block-copy-paste.directive';



@NgModule({
  declarations: [ConfirmDialogComponent, AppBlockCopyPasteDirective],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppBlockCopyPasteDirective
  ]
})
export class SharedModule { }
