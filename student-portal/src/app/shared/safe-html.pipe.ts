import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'safeHtml'
})
export class SafeHtmlPipe implements PipeTransform {
  imageToShow: string | ArrayBuffer;
  url = environment.host;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
  }

  transform(value: any, ...args: any[]): any {

    this.http.get(`${this.url}` + value, { responseType: 'blob' })
    .subscribe(data => {
      this.createImageFromBlob(data);
      return  this.imageToShow;
      }, error => {
        // this.isImageLoading = false;
        console.log(error);
      });
  }


  createImageFromBlob(image: Blob): any {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
       this.imageToShow = reader.result;
    }, false);
    if (image) {
       reader.readAsDataURL(image);
    }
 }

}
