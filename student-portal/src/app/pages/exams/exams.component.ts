import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Exam } from 'src/app/core/models';
import { ExamsService } from 'src/app/services/exams.service';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss']
})
export class ExamsComponent implements OnInit , AfterViewInit {
  displayedColumns: string[] = [
    'courseNumber', 'courseName',
    'from', 'to',
    'examNumber',
    'start'
  ];
  public dataSource;
  public exams: Exam[];
  duration = 2000;
  source = timer(0, 2000);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private route: ActivatedRoute, private examService: ExamsService, private router: Router) {
    this.dataSource = new MatTableDataSource(this.exams);

  }

  ngOnInit(): void {
     this.subscription = this.source.subscribe(val => {
      this.loadexams();
    });
  }

  private loadexams(): any {
    this.examService.getExamsForStudents().subscribe(res => {
      const current: Date = new Date();
      this.dataSource.data = res.filter(exam => {
        const start = new Date(exam.start);
        const end = new Date(exam.end);
        if (current.getTime() > end.getTime()) {
          return current.getTime() < end.getTime();
        }
        return current.getTime() >= start.getTime();
      });

    });
  }

  startTheExam(examid): void {
    this.subscription.unsubscribe();
    this.router.navigate(['/exam-view'], examid);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy(): any {
    this.subscription.unsubscribe();
 }

 ngAfterViewInit(): void {
  this.dataSource.paginator = this.paginator;
}
}

