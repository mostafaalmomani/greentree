import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MarksService } from 'src/app/services/marks.service';
import { Filter, StudentMarksObj } from 'src/app/core/models';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.scss']
})
export class MarksComponent implements OnInit , AfterViewInit{
  courseNumber = 1;
  displayedColumns: string[] = [
    'courseName', 'courseNumber', 'firstExamMark',
    'secondExamMark',  'otherExamMark',
    'sum', 'order', 'avg'
  ];
  displayedColumns2: string[] = [
    'courseNumberM', 'courseNameM',
    'grade', 'courseStatus'
  ];

  public semsters: any[] = [
    { value: '1', viewValue: 'أول' },
    { value: '2', viewValue: 'ثاني' },
    { value: '3', viewValue: 'صيفي' },
  ];
  public dataSource;
  public dataSource2;
  public marksObj = [];
  public dataSourseObj: StudentMarksObj[] = [];
  assessmentObj: any;
  assessments = [];
  coursesMark = [];
  firstMark: any;
  secondMark: any;
  otherMark: any;
  sum: any;
  public status: any;
  source = timer(0, 9000);
  subscription: any;
  @ViewChild('assesmentPaginator') assesmentPaginator: MatPaginator;
  @ViewChild('coursePaginator') coursePaginator: MatPaginator;
  filter: Filter = new Filter();
  filterFormGroup: FormGroup;
  filter2FormGroup: FormGroup;
  years = [];
  filter2: Filter = new Filter();
  constructor(private marksService: MarksService, private formBuilder: FormBuilder) {
    this.dataSource = new MatTableDataSource(this.assessments);
    this.dataSource2 = new MatTableDataSource(this.coursesMark);
  }

  ngOnInit(): void {
     this.yearsRange();
     this.createFilter();
     this.createFilter2();
    //  this.subscription = this.source.subscribe(val => {
    //   this.loadCourseMark();
    //  // this.loadMarks();
    //  });
  }

  private createFilter(): void {
    this.filterFormGroup = this.formBuilder.group({
      semester: new FormControl('', [Validators.required]),
      // courseName: new FormControl(''),
      year: new FormControl('', [Validators.required]),
    });
  }

  private createFilter2(): void {
    this.filter2FormGroup = this.formBuilder.group({
      semester: new FormControl('', [Validators.required]),
      // courseName: new FormControl(''),
      year: new FormControl('', [Validators.required]),
    });
  }

  public loadCourseMark(): any {
    this.filter2 = this.filter2FormGroup.getRawValue();
    this.marksService.getCoursesMark(this.filter2)
      .subscribe(arg => {
        this.coursesMark = arg.Markcourses;
        this.dataSource2.data = this.coursesMark;
      });
  }

  public loadMarks(): any {
    this.filter = this.filterFormGroup.getRawValue();
    // const year = this.filterFormGroup.get('year').value;
    // this.filter.year =   (new Date(year)).getFullYear();
    this.marksService.getMarks(this.filter)
      .subscribe(res => {
        this.assessments = res.Assessments;
        this.dataSource.data = this.assessments;
        this.assessmentObj = this.assessments;
      });
  }
  kindOfExamFilter(e): void {
    this.filter = e.value;
    this.loadMarks();
  }
  ngOnDestroy(): any {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.assesmentPaginator;
    this.dataSource2.paginator = this.coursePaginator;
  }

  yearsRange(): any {
    let i = 0;
    for (let index = new Date().getFullYear(); index > 2000; index--) {
      this.years[i++] = index;
    }
  }
}


