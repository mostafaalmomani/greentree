import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BaseService } from '../services/base.service';

@Pipe({
  name: 'loadImage'
})
export class LoadImagePipe implements PipeTransform {

  imageToShow: SafeUrl | null = null;
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient, private sanitizer: DomSanitizer, private base: BaseService) {
  }

  transform(value: any, ...args: any[]): any {
    const headers = new Headers({ 'Content-Type': 'image/*' });
    return `${this.base.apiUrl}readImage/${value}`;

  }


  convertBlobToBase64(blob: Blob): any {
    return Observable.create(observer => {
      const reader = new FileReader();
      const binaryString = reader.readAsDataURL(blob);
      reader.onload = (event: any) => {
        observer.next(event.target.result);
        observer.complete();
      };

      reader.onerror = (event: any) => {
        console.log('File could not be read: ' + event.target.error.code);
        observer.next(event.target.error.code);
        observer.complete();
      };
    });
  }

}
