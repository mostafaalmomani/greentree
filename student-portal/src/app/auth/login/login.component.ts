import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({});
  returnUrl = '';
  loggedIn = false;
  submitted = false;
  loading = false;

  constructor(private fb: FormBuilder, private route: ActivatedRoute,
              private router: Router, private authService: AuthService) {
                this.router.navigate(['/']);
              }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      number: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl =  '/';
  }
  get loginInfo(): any { return this.loginForm.controls; }

  onSubmit(): void {
    this.submitted = true;
    console.log(this.loginForm.value);
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authService.login(this.loginInfo.number.value, this.loginInfo.password.value)
      .pipe(first())
      .subscribe(data => {
        // location.reload();
        this.router.navigate([this.returnUrl]);
      }, (error) => {
        console.log(error.error);
        this.loading = false;
      });
  }
}
