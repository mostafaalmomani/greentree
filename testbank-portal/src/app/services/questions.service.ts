import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Student } from '../core/models';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService extends BaseService{
  activeQ;

  constructor(private http: HttpClient) {
    super();
  }

  getQuestionsForExam(): any{
    return this.http.get(`${this.apiUrl}getQuestionsForExam`, {
        params: {
          examID: this.examID
        }
    });
  }

  set activeQuestion(q){
    this.activeQ = q;
  }

  // tslint:disable-next-line:typedef
  get activeQuestion() {
    return this.activeQ;
  }

  // tslint:disable-next-line:typedef
  get examID(){
    return sessionStorage.getItem('examID');
  }

  getQuestion(qID): any{
    return this.http.get(`${this.apiUrl}getQuestion`, {
      params: {
        qID
      }
    });
  }

  saveAnswerForQuestion(answer, question): any{
    return this.http.post(`${this.apiUrl}saveAnswerForQuestion`, {
      answer,
      question
    });
  }

}
