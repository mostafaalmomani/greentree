import { Injectable } from '@angular/core';
import { Student } from '../core/models';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ExamsService extends BaseService{

  examQuestions;
  currentQ;
  currentExamInfo;

  constructor(private http: HttpClient, private auth: AuthService) {
    super();
  }


  getExams(): any{
    return this.http.get(`${this.apiUrl}getExamsForTestBank`, {
      params: {
        studentID: this.auth.getStudentId()
      }
    });
  }

  set currentQuestion(q){
    this.currentQ = q;
  }

  get currentQuestion(): any {
    return this.currentQ;
  }

  set currentExam(e){
    this.examQuestions = e;
  }

  get currentExam(): any {
    return this.examQuestions;
  }

}
