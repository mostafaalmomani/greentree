import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { PagesModule } from '../pages/pages.module';
import { SharedModule } from '../shared/shared.module';
import { BaseComponent } from './base.component';



@NgModule({
  declarations: [BaseComponent],
  imports: [
    AppRoutingModule,
    CommonModule,
    SharedModule,
    PagesModule
  ],
  entryComponents: [BaseComponent]
})
export class BaseModule { }
