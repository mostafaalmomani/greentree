import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }
  // tslint:disable-next-line:typedef
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    const currentStudent = this.authService.currentStudentValue;
    const examID = this.authService.currentExamID;
    if (currentStudent) {
      return true;
    }

    this.router.navigate(['/auth/login']);
    return false;
  }

}
