import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { BaseComponent } from './base/base.component';
import { ExamGuard } from './core/exam.guard';

const routes: Routes = [{
  path: 'auth',
  loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '',
    redirectTo: 'MainView/pages/exams',
    pathMatch: 'full'
  },
  {
    path: 'MainView',
    component: BaseComponent,
    canActivate: [AuthGuard],
    canLoad: [ExamGuard],
    children: [
      {
        path: 'pages',
        loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
