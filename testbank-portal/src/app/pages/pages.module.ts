import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/material/material.module';
import { HomeComponent } from './home/home.component';
import { SideQuestionsComponent } from './side-questions/side-questions.component';
import { LabValuesComponent } from './header/lab-values/lab-values.component';
import { QuestionComponent } from './home/question/question.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesRoutingModule } from './pages-routing.module';
import { DrawComponent } from './home/draw/draw.component';
import { ExamsPageComponent } from './exams-page/exams-page.component';
import { CountdownModule } from 'ngx-countdown';
import { LoadImagePipe } from './load-image.pipe';


@NgModule({
  declarations: [
    HomeComponent,
    QuestionComponent,
    HeaderComponent,
    LabValuesComponent,
    SideQuestionsComponent,
    FooterComponent,
    DashboardComponent,
    DrawComponent,
    ExamsPageComponent,
    LoadImagePipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    PagesRoutingModule,
    CountdownModule
  ],

  exports: [
    CountdownModule,
  ],
  bootstrap: [
    HomeComponent,
    QuestionComponent,
    HeaderComponent,
    LabValuesComponent,
    SideQuestionsComponent,
    FooterComponent,
    DrawComponent
  ]
})
export class PagesModule { }
