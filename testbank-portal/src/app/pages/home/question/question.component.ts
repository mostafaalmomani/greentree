import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { QuestionsService } from 'src/app/services/questions.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})


export class QuestionComponent implements OnInit, OnChanges {
  @Input() currentQuestion: Question;
  arrOfOptions = [];
  questionsTitle = '';
  correctAnswer = '';
  isItTheRightAnswer = false;
  showExplanation = false;
  showResults = true;
  showSaveButton = false;
  selectedAnswer: string;
  questionTotal: number;
  correctPercentage: number;
  constructor(private questionService: QuestionsService, private auth: AuthService) {
    // Todo get questions for  let examID = localStorage.getItem('examID')
  }

  ngOnChanges(): any {
    if (this.currentQuestion) {
      console.log(this.currentQuestion);
      this.showResults = this.showExplanation = this.showSaveButton = false;
      const q = this.currentQuestion;
      this.questionsTitle = q.questionText;
      this.correctAnswer = q.correctAnswer[0].text;
      this.arrOfOptions = [];
      this.showResults = false;
      q.answers.map(r => {
        this.arrOfOptions.push(
          {
            text: r.text,
            timesAnswered: r.timesAnswered ? r.timesAnswered : 0,
            rightAnswer: r.answer
          }
        );
      });
    }
  }

  answerSelected(answer): any {
    console.log(answer === this.correctAnswer);
    this.isItTheRightAnswer = answer === this.correctAnswer;
    this.showSaveButton = true;
    this.selectedAnswer = answer;
  }

  saveAnswerAndShowExplanation(): any  {
    this.showExplanation = this.showResults = true;
    this.questionService.
      saveAnswerForQuestion(this.
        updateAnswers(this.currentQuestion.answers, this.selectedAnswer), this.currentQuestion).subscribe((res: any) => {
      console.log(res);
      this.arrOfOptions = [];
      res.answers.map(r => {
        this.arrOfOptions.push(
          {
            text: r.text,
            timesAnswered: r.timesAnswered ? r.timesAnswered : 0,
            rightAnswer: r.answer
          }
        );
        if (r.answer) {
          this.correctPercentage = this.calcPercentage(res.total, r.timesAnswered);
        }
      });
      this.questionTotal = res.total;
    });
  }

  ngOnInit(): void {
    // ayy lmao
  }

  calcPercentage(total, timesAnswered): any  {
    const num = (timesAnswered / total) * 100 / 100;
    return Math.round((num + Number.EPSILON) * 100) / 100;
  }

  trimImagePath(path: string): any  {
    return path.substr(path.lastIndexOf('\\') + 1);
  }

  updateAnswers(answers: any[], selectedAnswer): any  {
    return answers.map((answer: Answer) => {
      if (answer.text === selectedAnswer) {
        return { ...answer, timesAnswered: answer.timesAnswered ? answer.timesAnswered + 1 : 1 };
      }
      else {
        return { ...answer, timesAnswered: answer.timesAnswered ? answer.timesAnswered : 0 };
      }
    });
  }

}


interface Question {
  questionType: string;
  questionText: string;
  examId: string;
  explanation: string;
  imageQ: string;
  point: number;
  sequenceQ: boolean;
  correctAnswer: any[];
  answers: any[];
}

interface Answer {
  text: string;
  answer: string;
  timesAnswered: number;
}
