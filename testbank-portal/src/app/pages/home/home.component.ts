import { Component, Input, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnChanges {
  @Input() props : { labValueOpen: boolean ; question: any;} = {labValueOpen: true, question: {}};
  constructor() { }
  displayedColumns: string[] = ['Blood, Plasma, Serum', 'name', 'Reference Range', ' Range	SI Reference Interval'];
  ngOnInit(): void {
  }

  ngOnChanges(){
    console.log(this.props);
  }

}
