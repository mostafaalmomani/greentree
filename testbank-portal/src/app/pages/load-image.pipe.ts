import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'loadImage'
})
export class LoadImagePipe implements PipeTransform {
  apiUrl = environment.apiUrl;
  // apiUrl = 'http://localhost:80/api/v2/';
  constructor() {
  }

  transform(value: any, ...args: any[]): any {
    return `${this.apiUrl}readImage/${value}`;
  }

}
