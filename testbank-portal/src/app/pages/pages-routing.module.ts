import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExamsPageComponent } from './exams-page/exams-page.component';
import { AuthGuard } from '../core/auth.guard';
import { ExamGuard } from '../core/exam.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'exams'
  },
  {
    path: 'exams',
    component: ExamsPageComponent,
    canActivate: [AuthGuard],
  },
  {
  path : 'dashboard',
  component : DashboardComponent,
  canActivate: [AuthGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
