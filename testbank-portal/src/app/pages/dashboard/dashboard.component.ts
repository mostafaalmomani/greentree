import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service'
import { Router } from '@angular/router'
import { ExamsService } from 'src/app/services/exams-service.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  labValue;
  currentQ;
  obj = { labValueOpen: false, question: undefined }
  constructor(private authService: AuthService,private examsService: ExamsService, private route: Router) { 
    if(!authService.currentExamID)
      route.navigateByUrl('MainView/pages/exams')
  }

  ngOnInit(): void {
    
  }

  gettingLabValuesAndSendingItToHome(e){
    this.obj.labValueOpen=e;
  }

  gettingQuestionAndSedingItToHome(e){
    this.obj.question = e;
  }

}
