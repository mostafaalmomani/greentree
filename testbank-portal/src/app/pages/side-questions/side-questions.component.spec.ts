import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideQuestionsComponent } from './side-questions.component';

describe('SideQuestionsComponent', () => {
  let component: SideQuestionsComponent;
  let fixture: ComponentFixture<SideQuestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideQuestionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
