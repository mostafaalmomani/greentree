import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { QuestionsService } from 'src/app/services/questions.service';
import { ExamsService } from 'src/app/services/exams-service.service';

@Component({
  selector: 'app-side-questions',
  templateUrl: './side-questions.component.html',
  styleUrls: ['./side-questions.component.scss']
})
export class SideQuestionsComponent implements OnInit {
  @Output() newQuestion = new EventEmitter();
  questions=[];
  acitveQ = 0;
  constructor(private questionService: QuestionsService, private examsService: ExamsService) {
      questionService.getQuestionsForExam().subscribe(res=>{
        this.questions = res as [];
        this.examsService.currentExam = this.questions.length;
        if(this.questions.length > 0)
        this.getQuestion(0);
      })
   }

   getQuestion(index){
    this.acitveQ = index;
    this.examsService.currentQuestion = this.acitveQ + 1;
     let qID = this.questions[index];
     this.questionService.getQuestion(qID).subscribe((data:any)=>{
      this.questionService.activeQuestion = data[0];
      console.log('question over here: ', data);
      this.newQuestion.emit(data[0]);
     })
   }


  ngOnInit(): void {
  }

}
