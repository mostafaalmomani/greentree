import { Component, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-lab-values',
  templateUrl: './lab-values.component.html',
  styleUrls: ['./lab-values.component.scss']
})
export class LabValuesComponent implements OnInit {
  @Output() labValues = new EventEmitter<boolean>();
  constructor() { }
  toggledVal = false;
  ngOnInit(): void {
  }
  addTrueVal(): any{
    this.toggledVal = ! this.toggledVal;
    this.labValues.emit(this.toggledVal)
  }

}
