import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ExamsService } from 'src/app/services/exams-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() headerLabValues= new EventEmitter<boolean>();
  constructor(private auth: AuthService, public examsService: ExamsService) { }

  ngOnInit(): void {
  }
  sendInput(e){
    this.headerLabValues.emit(e);
  }
  logout(){
    this.auth.logout();
  }
}
