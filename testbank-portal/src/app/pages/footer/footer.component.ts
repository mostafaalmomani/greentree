import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ExamsService } from 'src/app/services/exams-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
 timeLeft: number;

  constructor(private authService: AuthService, private examsService: ExamsService, private route: Router) { }

  ngOnInit(): void {
    if (!this.examsService.currentExamInfo){
      this.route.navigateByUrl('MainView/pages/exams');
      return;
    }
    this.timeLeft = this.converter(this.examsService.currentExamInfo.duration);
  }

  closeExam(): any{
    this.authService.closeExam();
  }

  converter = (time) => {
    const [h, m] =  time.split(':');
    return (Number(h)  * 3600 ) + (Number(m) * 60);
  }

  onTimerFinished(e: Event): void {
    // tslint:disable-next-line:no-string-literal
    if (e['action'] === 'done'){
      this.closeExam();
    }
  }

}
