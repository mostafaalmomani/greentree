import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ExamsService } from 'src/app/services/exams-service.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-exams-page',
  templateUrl: './exams-page.component.html',
  styleUrls: ['./exams-page.component.scss']
})
export class ExamsPageComponent implements OnInit , AfterViewInit{
  username: string;
  exams: any = [];
  displayedColumns: string[] = [
    'courseNumber', 'courseName', 'examDate',
    'start'
  ];
  public dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private examsService: ExamsService, private route: Router, private authService: AuthService) {
    this.dataSource = new MatTableDataSource(this.exams);
   }
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.username = sessionStorage.getItem('fullName');
    this.examsService.getExams().subscribe((res: any) => {
      this.exams = res;
      this.dataSource.data = res;
    });
  }

  getQsForExam(examID): any {
    this.examsService.currentExamInfo = this.exams.filter(exam => exam._id === examID)[0];
    sessionStorage.setItem('examID', examID);
    this.route.navigateByUrl('MainView/pages/dashboard');
  }

  logout(): void {
    this.authService.logout();
  }

}
