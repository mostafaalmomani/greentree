import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppBlockCopyPasteDirective } from './app-block-copy-paste.directive';



@NgModule({
  declarations: [AppBlockCopyPasteDirective],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppBlockCopyPasteDirective
  ]
})
export class SharedModule { }
