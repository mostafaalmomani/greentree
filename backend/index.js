if (process.env.NODE_ENV !== 'production') require('dotenv').config();
// const https = require('https');
const fs = require("fs");
const app = require('./src/app');
// let httpsOptions = {
//   key: fs.readFileSync('/etc/letsencrypt/live/example.com/privkey.pem'),
//   cert: fs.readFileSync('/etc/letsencrypt/live/example.com/cert.pem'),
//   ca: fs.readFileSync('/etc/letsencrypt/live/example.com/chain.pem')
// };
const PORT = process.env.PORT || 80;

// const httpsServer = https.createServer(app);

var server = app.listen(PORT, function () {
  var port = server.address().port;
  console.log(`Express is working on http://localhost:${PORT}`);
});
// httpsServer.listen(PORT, function () {
//   var port = httpsServer.address().port;
//   console.log(`Express is working on http://localhost:${PORT}`);
// });

