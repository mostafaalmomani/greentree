const TeacherSchema = require('../models/teacher.model');
exports.createSuperAdmin = () => {
    try {
        TeacherSchema.countDocuments().then(docCount => {
            if(docCount > 0){
                console.log('SuperAdmin already created ');
            } else {
                let defaultSuperAdmin = new TeacherSchema({
                    fullName: process.env.fullName,
                    email: process.env.email.toLowerCase(), 
                    password: process.env.password,
                    phoneNumber: process.env.phoneNumber,
                    purePassword: process.env.purePassword,
                });
        
                defaultSuperAdmin.save().then(doc => {
                    console.log('SuperAdmin created successfully ');
                })
            }
        });

    } catch (error) {

    }
};