const express = require('express');
const router = express.Router();
const teacherController = require('../controllers/teacher.controller');
const questionController = require('../controllers/question.controller');
const studentController = require('../controllers/student.controller');
const examController = require('../controllers/exam.controller');
const testBankController = require('../controllers/test.bank.controller');
const imageController = require('../controllers/file.reader.controller');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart({ uploadDir: __dirname+ '/../uploads/profile image' });
const multipartMiddlewareStudentAnswers = multipart({ uploadDir: __dirname + '/../uploads/answers' });

const checkAuth = require('../config/passport.student');
router.get('/session/expired', (req, res, next) => {
    res.send({ active: req.session })
});

const validatePayloadMiddleware = (req, res, next) => {
    if (req.body) {
        next();
    } else {
        res.status(403).send({
            errorMessage: 'You need a payload'
        });
    }
};

// Auth and Sign Up
router.post('/auth/student', validatePayloadMiddleware,studentController.login);

// ---------------------------- teacher public Routes --------------------------------
router.post('/auth', validatePayloadMiddleware, studentController.login);

router.get('/logout', function (req, res) {
    res.status(200).send({ isLoggedOut: true });
});
//------------------- session config --------------------------------------
const authMiddleware = (req, res, next) => {
    console.log(req.isAuthenticated())
    
    if (req.isAuthenticated()) {
        next();
    } else {
        res.status(403).send({
            errorMessage: 'You must be logged in.'
        });
    }
};
// -------------- Protected Routes ----------------- //

// -------------- student routs ----------------- // getStudentsExamMark
router.get('/getStudentsMark/:studentId',checkAuth, studentController.getAcademicStudentInformation);
router.get('/getExamsForStudents/:studentId',checkAuth, studentController.getExamsForStudent);
router.get('/getStudentsCourse/:courseNumber',checkAuth, studentController.getCourses);
router.get('/getStudentsExamMark/:courseNumber/:studentId',checkAuth, studentController.getMarks);
router.get('/getCoursesStudents',checkAuth, examController.getCoursesStudents);
router.get('/getQuestions/:examId',checkAuth, questionController.getQuestions);
router.get('/getExamInfo/:examId',checkAuth, examController.getExamInfo);
router.post('/saveAnswers/:examId',checkAuth,examController.saveAnswers);
router.get('/getExamsForTestBank', checkAuth, testBankController.getExamsForTestBank);
router.get('/getQuestionsForExam', checkAuth, testBankController.getQuestionsForExam);
router.get('/getQuestion', checkAuth, testBankController.getQuestion);
router.get('/readImage/:path',imageController.get); 
router.get('/getMark/:markId', checkAuth, examController.getMark)
router.get('/isActive/:examId', checkAuth, studentController.isExamActive)
router.post('/saveAnswerForQuestion', checkAuth, testBankController.saveAnswerForQuestion)
router.post('/saveSAnswers', checkAuth,multipartMiddlewareStudentAnswers, examController.saveAnswersFile);
router.get('/getCoursesMark', checkAuth, studentController.getCoursesMark);


module.exports = router;