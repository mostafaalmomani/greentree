const mongoose = require('mongoose');

const { Schema } = mongoose;

const MarkCourseSchema = Schema({
    markDesc: { type: String, required: false, default: 'def' },
    examStatus: { type: Boolean, required: false, default: false },
    courseNumber: { type: String, required: true },
    courseName: { type: String, required: true },
    studentsId: { type: mongoose.Schema.Types.ObjectId, required: true },
    markPoint: { type: String, required: false },
    grade: { type: Number, required: false },
    semester: { type: Number, required: false, defaultValue: 1  },
    created: { type: Date, default: new Date() },
    isConfirmed: { type: Boolean, default:false },
    teacherId: { type: mongoose.Schema.Types.ObjectId, required: true },
    exam: { type: mongoose.Schema.Types.ObjectId, required: false, index: true, unique: false, sparse: true, ref: 'Exam' }

});

const MarkCourse = mongoose.model('MarkCourse', MarkCourseSchema);
module.exports = MarkCourse;