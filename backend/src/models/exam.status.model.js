const mongoose = require('mongoose');

const { Schema } = mongoose;

const ExamStatusSchema = Schema({
  examId: { type: mongoose.Schema.Types.ObjectId, required: true , ref: 'Exam'},
  studentId: { type: mongoose.Schema.Types.ObjectId, required: true , ref: 'Student'},
  isActive: { type: Boolean, required: false, default: true },
  examDate: { type: Date, required: false },
  TimeSpam: { type: Date, default: new Date().getDate() }
});


const ExamStatus = mongoose.model('ExamStatus', ExamStatusSchema);
module.exports = ExamStatus;