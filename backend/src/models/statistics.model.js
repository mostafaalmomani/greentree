const mongoose = require('mongoose');

const { Schema } = mongoose;

const statisticsSchema = Schema({
  facilityIndex: { type: Number, required: true },
  DifficultyFactor: { type: Number, required: true },
  created: { type: Date, required: true },
  QuestionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Questions'
  }
});

const statistics = mongoose.model('statistics', statisticsSchema);
module.exports = statistics;