const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const { Schema } = mongoose;

const QuestionsSchema = Schema({
  questionType: { type: String, required: true, unique: false, default: 'regular',index:true, sparse:true },
  questionText: { type: String, required: true, unique: false ,index: true, sparse:true  },
  answers: {type: Object, unique: false, required: false},
  correctAnswer: { type: Object, required: true },
  point: { type: Number, required: false, default: 0 },
  examId: { type: mongoose.Schema.Types.ObjectId, required: true },
  sequenceQ: { type: Boolean, required: false },
  imageQ: {
    type: String, required: false, default: 'non'
  },
  explanation: { type: String, required: false },
  TimeSpent: { type: Date, required: false },
  created: { type: Date, default: new Date() },
  total: { type: Number, default: 0 }
});

const Questions = mongoose.model('Questions', QuestionsSchema);
module.exports = Questions;