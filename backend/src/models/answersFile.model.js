const mongoose = require('mongoose');

const { Schema } = mongoose;

const StudentAnswersSchema = Schema({
    examId: { type: mongoose.Schema.Types.ObjectId, required: true },
    courseNumber: { type: String, required: true },
    studentsId: { type: mongoose.Schema.Types.ObjectId, required: true },
    path: { type: String, required: false },
    created: { type: Date, default: new Date() },
});

const StudentAnswers = mongoose.model('StudentAnswers', StudentAnswersSchema);
module.exports = StudentAnswers;